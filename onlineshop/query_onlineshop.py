import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'onlineshop.settings')

import django
django.setup()
from django.core.files import File
from shop.models import Category, Product

#Compruebe  si  existe  un usuario  con  id=10
# en caso  contrario  crear  el  usuario


#creacion categoria ofertas
try:
	ofertas = Category.objects.get_or_create(catName="ofertas")[0]
except:
	print "No se ha podido crear la categoria ofertas"

#Creacion de oferta 1
prodName = "oferta 1"
description= "El perfume para hombres"
price= 6
stock= 5
availability= True
image_name="nenuco.jpg"
imageObject = File(open(os.path.join("media/", image_name),'r'))
try:
	oferta1 = Product.objects.get_or_create(category=ofertas, prodName=prodName, description=description, price=price, stock=stock, availability=availability)[0]
	oferta1.image.save(image_name, imageObject, save = True)
	oferta1.save()
except:
	print "No se ha podido crear correctamente oferta 1"
#Creacion de oferta 2
prodName = "oferta 2"
description= "Solo para leyendas"
price= 3
stock= 11
availability= True
image_name="nenuco.jpg"
imageObject = File(open(os.path.join("media/", image_name),'r'))
try:
	oferta2 = Product.objects.get_or_create(category=ofertas, prodName=prodName, description=description, price=price, stock=stock, availability=availability)[0]
	oferta2.image.save(image_name, imageObject, save = True)
	oferta2.save()
except:
	print "No se ha podido crear correctamente oferta 2"
#Creacion de oferta 3
prodName = "oferta 3"
description= "El perfume para hombres"
price= 12
stock= 14
availability= True
image_name="nenuco.jpg"
imageObject = File(open(os.path.join("media/", image_name),'r'))
try:
	oferta3 = Product.objects.get_or_create(category=ofertas, prodName=prodName, description=description, price=price, stock=stock, availability=availability)[0]
	oferta3.image.save(image_name, imageObject, save = True)
	oferta3.save()
except:
	print "No se ha podido crear correctamente oferta 3"
ofertas.save()

#creacion categoria gangas
try:
	gangas = Category.objects.get_or_create(catName="gangas")[0]
except:
	print "No se ha podido crear la categoria gangas"

#Creacion de ganga 1
prodName = "ganga 1"
description= "Lo gozaras"
price= 17
stock= 14
availability= True
image_name="212man.jpg"
imageObject = File(open(os.path.join("media/", image_name),'r'))
try:
	ganga1 = Product.objects.get_or_create(category=gangas, prodName=prodName, description=description, price=price, stock=stock, availability=availability)[0]
	ganga1.image.save(image_name, imageObject, save = True)
	ganga1.save()
except:
	print "No se ha podido crear correctamente ganga 1"
#Creacion de ganga 2
prodName = "ganga 2"
description= "Para gente electrica"
price= 20
stock= 1
availability= True
image_name="212man.jpg"
imageObject = File(open(os.path.join("media/", image_name),'r'))
try:
	ganga2 = Product.objects.get_or_create(category=gangas, prodName=prodName, description=description, price=price, stock=stock, availability=availability)[0]
	ganga2.image.save(image_name, imageObject, save = True)
	ganga2.save()
except:
	print "No se ha podido crear correctamente ganga 2"
#Creacion de ganga 3
prodName = "ganga 3"
description= "Para gente que no le gusta perder"
price= 4
stock= 13
availability= True
image_name="212man.jpg"
imageObject = File(open(os.path.join("media/", image_name),'r'))
try:
	ganga3 = Product.objects.get_or_create(category=gangas, prodName=prodName, description=description, price=price, stock=stock, availability=availability)[0]
	ganga3.image.save(image_name, imageObject, save = True)
	ganga3.save()
except:
	print "No se ha podido crear correctamente ganga 3"
gangas.save()

#EL resultado de la query saca los resultados de Gangas, se podria hacer en varias lineas


try:
	gangas = Product.objects.filter(category_id = Category.objects.get(catName = "gangas").id)
	print gangas
except:
	print "No realiza bien la query que obtiene las gangas dado el catName"


#Query que obtiene la categoria dado el prodSlug

try:
	idcategoria = Product.objects.get(prodSlug = "oferta-1").category_id
	categoria = Category.objects.get(id = idcategoria)
	print categoria.catSlug
except:
	print "No obtiene bien la categoria dado el prodSlug"

#Query

try:
	idcategoria = Product.objects.get(prodName = "oferta_10").category_id
	categoria = Category.objects.get(id = idcategoria)
	print categoria.catSlug
except:
	print "Producto oferta_10 inexistente"
