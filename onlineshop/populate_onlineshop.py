import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'onlineshop.settings')

import django
django.setup()
from shop.models import Category, Product
from django.core.files import File

def populate():
     # First, we will create lists of dictionaries containing the pages
     # we want to add into each category.
     # Then we will create a dictionary of dictionaries for our categories.
     # This might seem a little bit confusing, but it allows us to iterate
     # through each data structure, and add the data to our models.

    colonias = [
        { "prodName":"Diesel n1",
          "image_name": "diesel.jpg",
          "description": "Producto de diesel tanto para hombres como para mujeres",
          "price": "10",
          "stock": "2",
          "availability": "True",},

        { "prodName":"Diesel n2",
          "image_name": "diesel.jpg",
          "description": "Chloe es una fragancia de la familia olfativa Oriental Floral",
          "price": "16",
          "stock": "9",
          "availability": "True",},

        { "prodName":"Diesel n3",
          "image_name": "diesel.jpg",
          "description": "Producto de la famosa marca de perfumes de Calvin Klein",
          "price": "22",
          "stock": "13",
          "availability": "True",},

        { "prodName":"Chanel n5",
          "image_name": "diesel.jpg",
          "description": "Perfume estrella de la casa de moda chanel y de gran importancia en el mercado de los perfumes florales",
          "price": "7",
          "stock": "17",
          "availability": "True",},

        { "prodName":"Diesel fuel for life",
          "image_name": "diesel.jpg",
          "description": "Fragancia de moda de la marca de moda Only the Brave y una de las fragancias para hombre mas vendiadas",
          "price": "8",
          "stock": "1",
          "availability": "True",},

        { "prodName":"Aqua di gio",
          "image_name": "diesel.jpg",
          "description": "El ultimo producto de giorgio armani, marca caracterizada por la calidad de su ropa",
          "price": "15",
          "stock": "0",
          "availability": "False",}]

    perfumes = [
	    { "prodName":"Victor secret",
	      "image_name": "VictorSecret.jpg",
	      "description": "Producto de victor secret, para gente con secretos",
	      "price": "15",
	      "stock": "4",
	      "availability": "True",},

	    { "prodName":"Amethist",
	      "image_name": "amethist.jpg",
	      "description": "Perfume exclusivo para mujeres que les gusta oler bien",
	      "price": "12",
	      "stock": "5",
	      "availability": "True",},

	    { "prodName":"blackopium",
	      "image_name": "blackopium.jpg",
	      "description": "La religion es el opio para el pueblo",
	      "price": "13",
	      "stock": "6",
	      "availability": "True",},

	    { "prodName":"jadore",
	      "image_name": "jadore.jpg",
	      "description": "La adoraras",
	      "price": "14",
	      "stock": "8",
	      "availability": "True",},

	    { "prodName":"invictus",
	      "image_name": "invictus.jpg",
	      "description": "Para gente que no le gusta perder",
	      "price": "16",
	      "stock": "1",
	      "availability": "True",},

	    { "prodName":"invictus",
	      "image_name": "invictus.jpg",
	      "description": "Para gente que no le gusta perder",
	      "price": "16",
	      "stock": "1",
	      "availability": "True",}]

    tercer_lote = [
        { "prodName":"Electric",
          "image_name": "calvinklein.jpg",
          "description": "Para gente electrica",
          "price": "7",
          "stock": "2",
          "availability": "True",},

        { "prodName":"Nenuco",
          "image_name": "nenuco.jpg",
          "description": "La de siempre",
          "price": "1",
          "stock": "1",
          "availability": "True",},

        { "prodName":"loewe7",
          "image_name": "loewe7.jpg",
          "description": "Colonia pa pijos",
          "price": "6",
          "stock": "2",
          "availability": "True",},

        { "prodName":"sausagge",
          "image_name": "sauvage.jpg",
          "description": "Para la gente muy savage",
          "price": "60",
          "stock": "100",
          "availability": "True",},

        { "prodName":"leyenda",
          "image_name": "leyenda.jpg",
          "description": "Solo las leyendas lo huelen",
          "price": "14",
          "stock": "25",
          "availability": "True",},

        { "prodName":"212man",
          "image_name": "212man.jpg",
          "description": "Podria oler peor",
          "price": "11",
          "stock": "11",
          "availability": "True",},]

#encina -- evento, huele a musgo de encina


    cats = {"Colonias": {"product": colonias},
    		"Perfumes": {"product": perfumes},
    		"Fragancia": {"product": tercer_lote}}

     # If you want to add more catergories or pages,
     # add them to the dictionaries above.

     # The code below goes through the cats dictionary, then adds each category,
     # and then adds all the associated pages for that category.
     # if you are using Python 2.x then use cats.iteritems() see
     # http://docs.quantifiedcode.com/python-anti-patterns/readability/
     # for more information about how to iterate over a dictionary properly.

    for cat, cat_data in cats.items():
         c = add_cat(cat)
         for p in cat_data["product"]:
             add_product(c, p["prodName"], p["image_name"], p["description"], p["price"], p["stock"], p["availability"])

     # Print out the categories we have added.
    for c in Category.objects.all():
         for p in Product.objects.filter(category=c):
             print("- {0} - {1}".format(str(c), str(p)))

def add_product(cat, prodname, image_name, description, price, stock, availability):
     imageObject = File(open(os.path.join("static/images/", image_name),'r'))
     p = Product.objects.get_or_create(category=cat, prodName=prodname, description=description, price=price, stock=stock, availability=availability)[0]
     p.image.save(image_name, imageObject, save = True)
     p.save()
     return p

def add_cat(catName):
     c = Category.objects.get_or_create(catName=catName)[0]
     c.save()
     return c

# Start execution here!
if __name__ == '__main__':
    print("Starting Shop population script...")
    populate()
