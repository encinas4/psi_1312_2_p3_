# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse
from shop.models import Category, Product
# Create your views here.
#def index(request):
#	context_dict = {'boldmessage': "olor, olfato, sensualidad, perfume"}
#	return render(request, 'shop/index.html', context=context_dict)

# Vista base.html, el template de las páginas
#def base(request):
#	return render(request, 'shop/base.html')

def product_list(request, catSlug=None):

	#Your code goes here
	#queries that fill, category, categories and products

	categories = Category.objects.filter().all

	if(catSlug == None):
		category = None;
		products = Product.objects.filter().all

	else:
		category = Category.objects.get(catSlug = catSlug)
		products = Product.objects.filter(category = category).all
 
	return render(request,'shop/product_list.html', {'category': category, 'categories': categories, 'products': products})

def product_detail(request, id, prodSlug):

	product = Product.objects.get(id = id);
	return render(request, 'shop/product_detail.html', {'product': product})
