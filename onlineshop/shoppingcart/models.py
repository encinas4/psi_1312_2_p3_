# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class ShoppingCart(models.Model):
    product = models.ForeignKey(Product, null=False, unique=True)
    cart = models.ForeignKey()



class Cart(models.Model):
    units = models.IntegerField(default=1, null=False)
    price = models.DecimalField(max_digits=10, decimal_places=2, null=False)
